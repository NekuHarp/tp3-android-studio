package com.example.uapv1804546.tp3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

public class MainActivity extends AppCompatActivity {

    private WeatherDbHelper wd;
    private WeatherAdapter wa;
    private Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Context context = this;

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), NewCityActivity.class);
                startActivityForResult(i, 2);
            }
        });
        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new async().execute("");
            }
        });
        WeatherDbHelper dbHelper = new WeatherDbHelper(this);
        dbHelper.populate();
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this,android.R.layout.simple_expandable_list_item_2,dbHelper.fetchAllCities(), new String[] { WeatherDbHelper.COLUMN_CITY_NAME, WeatherDbHelper.COLUMN_COUNTRY}, new int[] { android.R.id.text1, android.R.id.text2});
        wd = new WeatherDbHelper(this);

        final Cursor c = wd.fetchAllCities();

        final ListView listview = (ListView) findViewById(R.id.listView);

        wa = new WeatherAdapter(this, c);

        listview.setAdapter(wa);
        listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listview.setItemChecked(2, true);

        registerForContextMenu(listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                City city = wd.cursorToCity(item);
                Intent i = new Intent(getApplicationContext(), CityActivity.class);
                i.putExtra(City.TAG, city);

                startActivityForResult(i, 1);

            }
        });

        listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(final AdapterView parent, final View view, final int position, final long id) {
                Cursor item = (Cursor) parent.getItemAtPosition(position);
                while(!item.equals(c)){
                    c.moveToNext();
                }
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle("Suppression de "+c.getString(1))
                        .setMessage("Voulez-vous supprimer "+c.getString(1)+" ?")
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                wd.deleteCity(c);
                                Toast t = Toast.makeText(getApplicationContext(), c.getString(1)+" supprimé", Toast.LENGTH_SHORT);
                                reload();
                                t.show();

                            }
                        })
                        .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) { // Retour de vue de ville
            reload();

        } else if (requestCode == 2) { // Ajout de ville
            if(resultCode==RESULT_OK) {
                WeatherDbHelper dbHelper = new WeatherDbHelper(this);
                dbHelper.addCity((City) data.getParcelableExtra("City"));
                reload();
            }
        } else {
            Log.v("aaaa", "aaaaaaaaaaaaaaaaaaaaaaaa");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void reload(){
        Cursor c = wd.fetchAllCities();
        wa.changeCursor(c);
        wa.notifyDataSetChanged();
    }

    private class async extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            Cursor c = wd.fetchAllCities();
            City city;
            while(c.moveToNext()){
                city = wd.cursorToCity(c);
                String name = city.getName();
                String country = city.getCountry();
                URL url;
                try {
                    url = WebServiceUrl.build(name, country);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    Log.e("async", "Bad URL (city / country ?)");
                    return null;
                }

                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("async", "No internet");
                }

                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                    JSONResponseHandler resp = new JSONResponseHandler(city);
                    resp.readJsonStream(in);

                } catch (UnknownHostException e) {
                    Log.e("async", "No internet");
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    urlConnection.disconnect();
                }
                wd.updateCity(city);
            }

            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            reload();
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}
